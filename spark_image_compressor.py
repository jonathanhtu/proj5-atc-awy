import pyspark
from pyspark import SparkContext
import cv2
import numpy as np 
import scipy as sp
import struct
from helper_functions import *
from constants import *


def quantizeBlock(var):
    if var[0][1] == "Y":
        return (var[0], (var[1][0], var[1][1], quantize_block(var[1][2], True, QF), var[1][3]))
    else:
        return (var[0], (var[1][0], var[1][1], quantize_block(var[1][2], False, QF), var[1][3]))

def dequantizeBlock(var):
    if var[0][1] == "Y":
        return (var[0], (var[1][0], var[1][1], quantize_block(var[1][2], True, QF, inverse = True), var[1][3]))
    else:
        return (var[0], (var[1][0], var[1][1], quantize_block(var[1][2], False, QF, inverse = True), var[1][3]))

def substract(block):
    return block.astype(np.float32) - 128

def add(block):
    block += 128
    block[block>255] = 255
    block[block<0] = 0
    return block

def flat_map(item):
    blocks = []
    im = item[1]
    Y = im[0].astype(np.float32) - 128
    Cr = im[1].astype(np.float32) - 128
    Cb = im[2].astype(np.float32) - 128
    image = item[2]
    height, width = np.array(image.shape[:2])
    image_id = item[0]

    no_vert_blocks = Y.shape[1] / 8
    no_horz_blocks = Y.shape[0] / 8
    for y in range(no_vert_blocks):
        for x in range(no_horz_blocks):
            x_pos = x * 8
            y_pos = y * 8
            subblock_Y =  ((image_id, "Y", (height, width)), 
                           (x_pos, y_pos, Y[x_pos:(x_pos+8),y_pos:(y_pos+8)], (Y.shape[0], Y.shape[1])))
            blocks.append(subblock_Y)

    no_vert_blocks = Cr.shape[1] / 8
    no_horz_blocks = Cr.shape[0] / 8
    for y in range(no_vert_blocks):
        for x in range(no_horz_blocks):
            x_pos = x * 8
            y_pos = y * 8
            subblock_Cr = ((image_id, "Cr", (height, width)), 
                           (x_pos, y_pos, Cr[x_pos:(x_pos+8),y_pos:(y_pos+8)], (Cr.shape[0], Cr.shape[1])))
            blocks.append(subblock_Cr)
            
    no_vert_blocks = Cb.shape[1] / 8
    no_horz_blocks = Cb.shape[0] / 8
    for y in range(no_vert_blocks):
        for x in range(no_horz_blocks):
            x_pos = x * 8
            y_pos = y * 8
            subblock_Cb = ((image_id, "Cb", (height, width)), 
                           (x_pos, y_pos, Cb[x_pos:(x_pos+8),y_pos:(y_pos+8)], (Cb.shape[0], Cb.shape[1])))
            blocks.append(subblock_Cb)

    return blocks

def reduce1(block1, block2):
    x_pos_2 =    block2[0]
    y_pos_2 =    block2[1]
    subblock_2 = block2[2]
    x_pos_1 =    block1[0]
    y_pos_1 =    block1[1]
    subblock_1 = block1[2]
    size =       block1[3]
    shape0 =     size[0]
    shape1 =      size[1]
    result =     np.zeros((shape0, shape1), np.float32)

    
    if (len(subblock_1) == 8 and len(subblock_2) == 8):
        result[x_pos_1:(x_pos_1+8), y_pos_1:(y_pos_1+8)] = subblock_1
        result[x_pos_2:(x_pos_2+8), y_pos_2:(y_pos_2+8)] = subblock_2

    elif (x_pos_1 == -1
          and len(subblock_2) == 8):
        subblock_1[x_pos_2:(x_pos_2+8), y_pos_2:(y_pos_2+8)] = subblock_2
        result = subblock_1

    elif (x_pos_2 == -1
          and len(subblock_1) == 8):
        subblock_2[x_pos_1:(x_pos_1+8), y_pos_1:(y_pos_1+8)] = subblock_1
        result = subblock_2

    elif (x_pos_1 == -1 and x_pos_2 == -1):
        result = subblock_1
        for k in range (0, shape1):
            for i in range (0, shape0):
                if (result[i][k] == 0):
                    result[i][k] = subblock_2[i][k]
    return (-1, -1, result, size)

def reduce2(pair1, pair2):
    type1 = pair1[0]
    type2 = pair2[0]
    size  = pair1[2]
    height = size[0]
    width = size[1]
    result = np.zeros((height, width, 3), np.uint8)
    if (type1 == 0):
        block1 = pair1[1]
        block2 = add(pair2[1])
        result = block1
        if (type2 == "Y"):
            result[:,:,0] = resize_image(block2, width, height)
        elif (type2 == "Cr"):
            result[:,:,1] = resize_image(block2, width, height)
        elif (type2 == "Cb"):
            result[:,:,2] = resize_image(block2, width, height)

    elif (type2 == 0):
        block2 = pair2[1]
        block1 = add(pair1[1])
        result = block2
        if (type1 == "Y"):
            result[:,:,0] = resize_image(block1, width, height)
        elif (type1 == "Cr"):
            result[:,:,1] = resize_image(block1, width, height)
        elif (type1 == "Cb"):
            result[:,:,2] = resize_image(block1, width, height)

    else:
        block2 = add(pair2[1])
        block1 = add(pair1[1])
        if (type1 == "Y"):
            result[:,:,0] = resize_image(block1, width, height)
        elif (type1 == "Cr"):
            result[:,:,1] = resize_image(block1, width, height)
        elif (type1 == "Cb"):
            result[:,:,2] = resize_image(block1, width, height)

        if (type2 == "Y"):
            result[:,:,0] = resize_image(block2, width, height)
        elif (type2 == "Cr"):
            result[:,:,1] = resize_image(block2, width, height)
        elif (type2 == "Cb"):
            result[:,:,2] = resize_image(block2, width, height)

    return (0, result, size)

    
### WRITE ALL HELPER FUNCTIONS ABOVE THIS LINE ###

def generate_Y_cb_cr_matrices(rdd):
    """
    THIS FUNCTION MUST RETURN AN RDD
    """
    ### BEGIN SOLUTION ###
    return rdd.map(lambda pair: (pair[0], convert_to_YCrCb(pair[1]), pair[1]))

def generate_sub_blocks(rdd):
    """
    THIS FUNCTION MUST RETURN AN RDD
    """
    ### BEGIN SOLUTION ###
    return rdd.flatMap(flat_map)

def apply_transformations(rdd):
    """
    THIS FUNCTION MUST RETURN AN RDD
    """
    ### BEGIN SOLUTION ###
    rdd = rdd.map(lambda tp: (tp[0], (tp[1][0], tp[1][1], dct_block(tp[1][2]), tp[1][3])))
    rdd = rdd.map(quantizeBlock)
    rdd = rdd.map(dequantizeBlock)
    rdd = rdd.map(lambda tp: (tp[0], (tp[1][0], tp[1][1], dct_block(tp[1][2], inverse = True), tp[1][3])))
    return rdd

def combine_sub_blocks(rdd):
    """
    Given an rdd of subblocks from many different images, combine them together to reform the images.
    Should your rdd should contain values that are np arrays of size (height, width).

    THIS FUNCTION MUST RETURN AN RDD
    """
    ### BEGIN SOLUTION ###
    rdd = rdd.reduceByKey(reduce1)
    rdd = rdd.map(lambda pair: (pair[0][0], (pair[0][1], pair[1][2], pair[0][2])))
    rdd = rdd.reduceByKey(reduce2)
    rdd = rdd.map(lambda pair: (pair[0], pair[1][1]))
    rdd = rdd.map(lambda pair: (pair[0], to_rgb(pair[1])))
    return rdd 
    
def run(images):
    """
    THIS FUNCTION MUST RETURN AN RDD

    Returns an RDD where all the images will be proccessed once the RDD is aggregated.
    The format returned in the RDD should be (image_id, image_matrix) where image_matrix 
    is an np array of size (height, width, 3).
    """
    sc = SparkContext()
    rdd = sc.parallelize(images, 16) \
        .map(truncate).repartition(16)
    rdd = generate_Y_cb_cr_matrices(rdd)
    rdd = generate_sub_blocks(rdd)
    rdd = apply_transformations(rdd)
    rdd = combine_sub_blocks(rdd)

    ### BEGIN SOLUTION HERE ###
    # Add any other necessary functions you would like to perform on the rdd here
    # Feel free to write as many helper functions as necessary
    return  rdd
